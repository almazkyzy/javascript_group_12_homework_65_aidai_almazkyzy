import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { WatchItem } from '../../shared/watch-item.model';

@Component({
  selector: 'app-watch-item',
  templateUrl: './watch-item.component.html',
  styleUrls: ['./watch-item.component.css']
})
export class WatchItemComponent implements OnInit {
  @Input() watchItem!: WatchItem;
  faTimes = faTimes;

  constructor() { }

  ngOnInit(): void {
  }

}
