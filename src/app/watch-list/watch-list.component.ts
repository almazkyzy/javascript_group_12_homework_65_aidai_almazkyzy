import { Component, OnDestroy, OnInit } from '@angular/core';
import { WatchItem } from '../shared/watch-item.model';
import { WatchItemService } from '../shared/watch-item.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-watch-list',
  templateUrl: './watch-list.component.html',
  styleUrls: ['./watch-list.component.css']
})
export class WatchListComponent implements OnInit, OnDestroy {
  watchItems!: WatchItem[];
  watchItemsChangeSubscription!: Subscription;
  watchItemsFetchingSubscription!: Subscription;
  isFetching: boolean = false;

  constructor(private watchItemService: WatchItemService) {
  }

  ngOnInit() {
    this.watchItems = this.watchItemService.getWatchItems();
    this.watchItemsChangeSubscription = this.watchItemService.watchItemsChange.subscribe((watchItems: WatchItem[]) => {
      this.watchItems = watchItems;
    });
    this.watchItemsFetchingSubscription = this.watchItemService.watchItemsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.watchItemService.fetchItems();
  }
  ngOnDestroy() {
    this.watchItemsChangeSubscription.unsubscribe();
    this.watchItemsFetchingSubscription.unsubscribe();
  }
}
