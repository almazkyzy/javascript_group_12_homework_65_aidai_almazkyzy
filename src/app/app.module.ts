import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { AddWatchItemComponent } from './add-watch-item/add-watch-item.component';
import { FormsModule } from '@angular/forms';
import { WatchListComponent } from './watch-list/watch-list.component';
import { WatchItemComponent } from './watch-list/watch-item/watch-item.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { WatchItemService } from './shared/watch-item.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    AddWatchItemComponent,
    WatchListComponent,
    WatchItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FontAwesomeModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [WatchItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
