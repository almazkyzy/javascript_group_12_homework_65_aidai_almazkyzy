import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WatchItemService } from '../shared/watch-item.service';

@Component({
  selector: 'app-add-watch-item',
  templateUrl: './add-watch-item.component.html',
  styleUrls: ['./add-watch-item.component.css']
})
export class AddWatchItemComponent implements OnInit {
  @ViewChild('nameInput') nameInput!: ElementRef;


  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  addWatchItem() {
    const name = this.nameInput.nativeElement.value;
    const body = {name};
    this.http.post('https://aidai-fa920-default-rtdb.firebaseio.com/watch-items.json', body).subscribe();
  }

}
