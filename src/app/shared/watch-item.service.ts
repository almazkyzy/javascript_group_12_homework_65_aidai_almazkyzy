import { WatchItem } from './watch-item.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()

export class WatchItemService {
  watchItemsChange = new Subject<WatchItem[]>();
  watchItemsFetching = new Subject<boolean>();

  private watchItems: WatchItem[] = [];

  constructor(private http: HttpClient) {
  }

  fetchItems() {
    this.watchItemsFetching.next(true);
    this.http.get<{[id:string]: WatchItem}>('https://aidai-fa920-default-rtdb.firebaseio.com/watch-items.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const itemData = result[id];
          const watchItem = new WatchItem(
            id,
            itemData.name,
          );
          return watchItem;
        });
      }))
      .subscribe(watchItems => {
        this.watchItems = watchItems;
        this.watchItemsChange.next(this.watchItems.slice());
        this.watchItemsFetching.next(false);
      }, error => {
        this.watchItemsFetching.next(false);
      });
  }

  getWatchItems(){
    return this.watchItems.slice();
  }



  // getWatchItem(index: number) {
  //   return this.watchItems[index];
  // }

  // addWatchItem(watchItem: WatchItem) {
  //   this.watchItems.push(watchItem);
  //   this.watchItemsChange.next(this.watchItems);
  // }

}
