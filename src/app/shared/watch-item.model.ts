export class WatchItem {
  constructor(
    public id: string,
    public name: string,
  ) {}
}
